import os
import braintree

# noinspection PyUnresolvedReferences
from .settings_common import *

os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
BASE_DIR = pathlib.Path(__file__).absolute().parent.parent

DEBUG = True
SECRET_KEY = '-------------------------'

BLENDER_ID = {
    # Configure the OAuth Callback URL at Blender ID to something like:
    # http://fund.local:8010/oauth/authorized
    'BASE_URL': 'https://gallien.kde.org',
    'OAUTH_CLIENT': '----------',
    'OAUTH_SECRET': '----------------------',
    'BADGER_API_SECRET': '--set-in-settings.py--',
}

GATEWAYS = {
    'braintree': {
        'environment': braintree.Environment.Sandbox,
        'merchant_id': '--------',
        'public_key': '-----------',
        'private_key': '----------------------',

        # Merchant Account IDs for different currencies.
        # Configured in Braintree: Account → Merchant Account Info.
        'merchant_account_ids': {
            'EUR': 'kdeeuro',
            'USD': 'kdeusd',
        }
    },
    # No settings, but a key is required here to activate the gateway.
    'bank': {},
}

# HTTP clients on those IP addresses get to see the Django Debug Toolbar.
INTERNAL_IPS = ['127.0.0.1']

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'default': {
            'format': '%(asctime)-15s %(levelname)8s %(name)s %(message)s'
        },
        'verbose': {
            'format': '%(asctime)-15s %(levelname)8s %(name)s %(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'default',  # Set to 'verbose' in production
            'stream': 'ext://sys.stderr',
        },
        'file': {
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'formatter': 'verbose',  # Set to 'verbose' in production
            'filename': '/var/log/apps/blender-fund.log',
            'when': 'W6',
            'backupCount': 8,
            'encoding': 'utf-8',
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
        },
    },
    'loggers': {
        'blender_fund': {'level': 'DEBUG'},
        'blender_fund_main': {'level': 'DEBUG'},
        'blender_notes': {'level': 'DEBUG'},
        'looper': {'level': 'DEBUG'},
        'blender_id_oauth_client': {'level': 'DEBUG'},
    },
    'root': {
        'level': 'WARNING',
        'handlers': [
            # Disable this in production:
            'console',
        ],
    }
}

# For development, dump email to the console instead of trying to actually send it.
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
# Alternatively, run python3 -m smtpd -n -c DebuggingServer -d '0.0.0.0:2525' and set
# EMAIL_PORT = 2525

# Used by Django to send error mails.
ADMINS = [
    ('Carl Schwan', 'carl@carlschwan.eu'),
]

# For collecting usage metrics
GOOGLE_ANALYTICS_TRACKING_ID = ''

ALLOWED_HOSTS = ['krita-fund.carlschwan.eu', 'fund.local']

SOCIALACCOUNT_AUTO_SIGNUP = True
SOCIALACCOUNT_EMAIL_AUTHENTICATION_AUTO_CONNECT = True

SOCIALACCOUNT_PROVIDERS = {
    'openid_connect': {
        "EMAIL_AUTHENTICATION": True,
        "SERVERS": [
            {
                'id': 'invent',
                'name': 'KDE Invent',
                "server_url": 'https://invent.kde.org',
                "EMAIL_AUTHENTICATION": True,
                "APP": {
                    'client_id': '____________',
                    'secret': '__________',
                },
            },
        ]
    }
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'krita_fund',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': '127.0.0.1',
        'PORT': '3306',
        'ATOMIC_REQUESTS': True,
        'TEST': {
            'CHARSET': 'utf8',
            'COLLATION': 'utf8_general_ci',
        }
    },
}

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR / 'templates-krita'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'blender_fund_main.context_processors.settings',
                'blender_fund_main.context_processors.page_id',
                'looper.context_processors.preferred_currency',
            ],
        },
    },
]

CRISPY_TEMPLATE_PACK = 'bootstrap3'

# Uncomment to enable Sentry in production.
#
# The send_default_pii parameter controls whether it sends personally
# identifyable information (such as user ids, usernames, cookies,
# authorization headers, ip addresses) to Sentry.
# import sys
#
# if sys.stderr.isatty():
#     print('Skipping Sentry initialisation because stderr is a TTY', file=sys.stderr)
# else:
#     import logging
#     import sentry_sdk
#     from sentry_sdk.integrations.django import DjangoIntegration
#     from sentry_sdk.integrations.logging import LoggingIntegration
#
#     # By default Sentry only sends ERROR level and above.
#     sentry_logging = LoggingIntegration(
#         level=logging.INFO,          # Capture this level and above as breadcrumbs
#         event_level=logging.WARNING  # Send this level and above as events
#     )
#     sentry_sdk.init(
#         dsn="https://xxxxxxxxxxxx@sentry.io/yyyyyyyyy",
#         send_default_pii=False,
#         integrations=[sentry_logging, DjangoIntegration()]
#     )
