from django import template
from django.contrib.humanize.templatetags.humanize import naturaltime

register = template.Library()

@register.filter(name='naturaltime_compact')
def naturaltime_compact(time):
    """A more compact version of the humanize naturaltime filter."""

    compact_time = naturaltime(time)

    # Replace non-breaking space with regular space so it can be adjusted per case.
    compact_time = compact_time.replace(u'\xa0', u' ')

    # Take only the first part, e.g. "3 days, 2h ago", becomes " 3d ago"
    compact_time = compact_time.split(',')[0]

    compact_time = compact_time.replace('a second ago', 'now')
    compact_time = compact_time.replace(' seconds', 's')
    compact_time = compact_time.replace('a minute', '1m')
    compact_time = compact_time.replace(' minutes', 'm')
    compact_time = compact_time.replace('1 hour', '1h') # After "X days, 1 hour"
    compact_time = compact_time.replace('an hour', '1h') # Exactly 1 hour.
    compact_time = compact_time.replace(' hours', 'h')
    compact_time = compact_time.replace('1 day', '1d')
    compact_time = compact_time.replace(' days ago', 'd ago')
    compact_time = compact_time.replace(' days', 'd ago')
    compact_time = compact_time.replace(' day', 'd')
    compact_time = compact_time.replace('a week', 'w')
    compact_time = compact_time.replace(' weeks', 'w')
    compact_time = compact_time.replace('1 month', '1mo')
    compact_time = compact_time.replace(' months', 'mo')
    compact_time = compact_time.replace('month', 'mo')
    compact_time = compact_time.replace('1 year', '1y')
    compact_time = compact_time.replace(' years', 'y')
    compact_time = compact_time.replace('years', 'y')

    return compact_time
