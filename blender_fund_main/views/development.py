from django.contrib.auth.decorators import login_required
from django.http import JsonResponse


@login_required()
def my_permissions(request) -> JsonResponse:
    permissions = request.user.get_all_permissions()
    return JsonResponse(sorted(permissions), safe=False)
