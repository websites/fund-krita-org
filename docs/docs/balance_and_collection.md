# Balance and Collection

When creating subscriptions, we do not rely on the automatic billing APIs provided by the Gateways.
Instead, we run a 'process' function at regular intervals. The function will evaluate existing
subscriptions, generate orders, perform transactions etc.