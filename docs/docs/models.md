# Models

The main elements in Looper are Gateway, Plan, Subscription and Customer. These element relate
to each other through a few secondary models. Here is a high level overview of the relationship
between models.

![](looper_subscription.svg)

## Customer

The Customer model has a one-to-one relationship with the User. A Customer record needs to be
created after the creation of a User and requires the User's full name and email. The Customer is
created by listening to a post_save signal on User creation.

## Gateway

The Gateway is the connection with a payment processing provider. Currently the each Gateway stores
a unique name, which is then matched to configuration values store in the settings file. In the
future we migh store such confguration as key-values in a GatewaySettings table.

## Plans

The blueprints used to configure and create an actual Subscription. Once a Subscription is created,
it can be edited, and it only keeps a symbolic reference with its original Plan.
This allows for the user to switch from automatic to manual payments, to change backend and so on.

## Plan Variations

While the Plan stores the key information (name, description, etc.), we rely on Plan Variations to
define the matrix of variations for each Plan. A Plan should have at least one variation. The five
variation parameters are:

* Currency
* Interval Unit (day, week, month or year)
* Interval Length
* Collection Method (automatic or manual)
* Price in Cents

Here is an example for a single Plan.

Price | Currency | Interval Unit | Interval Lenght | Collection |
----- | -------- | ------------- | --------------- | ---------- |
1000  | EUR      | month         | 1               | automatic  |
1200  | USD      | month         | 1               | automatic  |
1300  | EUR      | month         | 1               | manual     |
1700  | USD      | month         | 1               | manual     |

## Subscriptions

The actual Subscription, tied to a Customer. Ideally we assign a Subscription to a Customer only
once and we then keep editing it until it gets canceled. Even when canceled, a Subscription may be
restored.

## Orders

The customer places an order when he manually purchases an extra month of membership, or when he
adds some credit to his Balance.
The system can create an Order on behalf of the customer when charging a Subscription that is set
to auto-renew.

## Transactions

A collection of every operation that affects a customer's Balance. The Transaction
lifetime follows this schema:

* Creation (pending)
* Transaction (completed or failed)

A Transaction can not be refunded, only an Order can. This will create another
Transaction in the customer's record.
